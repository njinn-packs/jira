---
name: jira
title: Atlassian Jira
description: |
  Integration for Atlassian Jira based Products (Jira, Jira Core, Jira ServiceDesk).
  Supports both Cloud and Server instances.
config-schemes:
  connection:
    type: object
    title: Connection
    icon: fab fa-jira
    verify_path: actions.base:TestJiraConnection
    description: Connection Information including authentication.
    properties:
      url:
        type: string
        title: Jira URL
        required: false
        description: URL of the Jira Application (eg. 'https://jira.atlassian.com')
        order: 10
      username:
        type: string
        title: Username
        required: false
        description: User used to make the Jira Connection
        order: 20
      password:
        type: secret
        title: Password/API Key
        required: false
        description: This is either the password (Jira Server) or an API key (Jira Cloud)
        order: 30
      cloud:
        type: boolean
        title: Cloud
        default: false
        description: The connection points to an Atlassian Cloud instance
        order: 40
actions:
  CreateIssue:
    title: Create Issue
    description: Creates a new Issue
    path: actions.issues:CreateIssue
    icon: fab fa-jira
    parameters:
      summary:
        type: string
        title: Summary
        required: true
        description: Issue Summary / Headline
        order: 10
      project:
        type: string
        title: Project
        required: true
        description: Key of the Project in which the issue will be created.
        order: 20
        remote_data:
          action: actions.issues:GetProjects
          required:
            - connection
      issuetype:
        type: string
        title: Issue Type
        required: true
        description: Jira can be used to track many different types of issues. Typical types include Service Request, Task, IT Help.
        order: 30
      reporter:
        type: string
        title: Reporter
        required: false
        description: Jira User ID that should be set as the Issue Reporter.
        order: 40
      assignee:
        type: string
        title: Assignee
        required: false
        description: Jira User ID that should be set as the Issue Assignee. Leave empty for 'Unassigned'
        order: 50
      parent_key:
        type: string
        title: Parent issue key
        required: false
        description: If issue is to be created as a child issue, provide the parent key.
        order: 60
      description:
        type: string
        title: Description
        required: false
        description: Context of the issue. Supports Atlassian text formatting (eg. *strong*). See Atlassian help pages for more information.
        order: 70
      connection:
        $ref: "#connection"
        required: true
        order: 100
    output:
      key:
        title: Issue Key
        value: IT-1234
        description: The Key of the created issue.
  TransitionIssue:
    title: Transition Issue
    description: Transition an Issue between states
    path: actions.issues:TransitionIssue
    icon: fab fa-jira
    parameters:
      key:
        type: string
        title: Issue Key
        required: true
        description: Key of the issue to transition (eg. IT-1234)
        order: 10
      transition:
        type: string
        title: Transition
        required: true
        description: Name of the trasition to use (eg. Close Issue)
        order: 20
      comment:
        type: string
        title: Comment
        required: false
        description: Comment to add to the issue when performing the transition
        order: 30
      fields:
        type: object
        title: Set Fields on Transition
        order: 40
        properties:
          assignee:
            type: string
            title: Assignee
            required: false
            order: 10
          resolution:
            type: string
            title: Resolution
            required: false
            order: 20
          additional:
            type: string
            title: Additional Fields
            description: |
              Additional fields to be set as a JSON dict.
              eg. {"customfield_10021": "my custom value"}
            required: false
      connection:
        $ref: "#connection"
        required: true
        order: 50
  AddUser:
    title: Add User to Jira
    description: Add a user to a given Jira Instance
    path: actions.users:AddUser
    icon: fab fa-jira
    parameters:
      username:
        type: string
        title: Username
        required: true
        description: Name of the User
        order: 10
      fullname:
        type: string
        title: Full name
        required: true
        description: The users full name
        order: 20
      email:
        type: string
        title: E-Mail
        required: true
        description: EMail address to associate the user with
        order: 30
      connection:
        $ref: "#connection"
        required: true
        order: 50
  SearchIssues:
    title: Search Issues
    description: Search for Jira Issues with a JQL query
    path: actions.issues:SearchIssues
    icon: fab fa-jira
    parameters:
      jql:
        type: string
        title: JQL query
        description: Search query in JQL https://confluence.atlassian.com/jirasoftwarecloud/advanced-searching-764478330.html
        order: 10
      connection:
        $ref: "#connection"
        required: true
        order: 20
    output:
      issues:
        title: Issue Keys
        value: ["IT-1234", "IT-1235", "IT-1236"]
        description: List of issue keys that match the search query.
  UpdateIssue:
    title: Update Issue
    description: Update an Issue
    path: actions.issues:UpdateIssue
    icon: fab fa-jira
    parameters:
      key:
        type: string
        title: Issue Key
        required: true
        description: Key of the issue to update (eg. IT-1234)
        order: 10
      summary:
        type: string
        title: Summary
        required: false
        order: 20
      assignee:
        type: string
        title: Assignee
        description: Use user name for Jira Server and user account id for Jira Cloud
        required: false
        order: 30
      priority:
        type: string
        title: Priority
        required: false
        order: 50
      description:
        type: code
        lang: text
        title: Description
        required: false
        order: 60
      additional:
        type: code
        lang: json
        title: Additional Fields
        description: |
          Additional fields to be set as a JSON dict.
          eg. {"customfield_10021": "my custom value"}
        required: false
        order: 70
      comment:
        type: text
        title: Comment
        required: false
        description: Comment to add to the issue for the update
        order: 80
      connection:
        $ref: "#connection"
        required: true
        order: 100

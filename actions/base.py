import requests
from jira import JIRA, JIRAError


class JiraAction:
    def __init__(self):
        self.connection = None
        self.__jira_instance = None
        self.cloud = False

    @property
    def jira(self):
        if not self.__jira_instance:
            self.__jira_instance = self.jira_instance()
        return self.__jira_instance

    def jira_instance(self):

        kwargs = dict()

        if self.connection and self.connection.get("url"):
            kwargs["server"] = self.connection.get("url")

            if self.connection.get("username") and self.connection.get("password"):
                kwargs["basic_auth"] = (
                    self.connection.get("username"),
                    self.connection.get("password"),
                )
        else:
            response = self._njinn.get("jira/connection")
            if response.status_code == requests.codes.ok:
                kwargs = response.json()
            else:
                raise Exception(
                    "Could not get connection info from Njinn. Please provide the Jira Connection Information."
                    f"API replied with ({response.status_code}): {response.text}"
                )

        jira = JIRA(**kwargs)

        return jira


class TestJiraConnection:
    def __init__(self):
        self.connection = None

    def run(self):
        kwargs = dict()
        if self.connection and self.connection.get("url"):
            kwargs["server"] = self.connection.get("url")

            if self.connection.get("username") and self.connection.get("password"):
                kwargs["basic_auth"] = (
                    self.connection.get("username"),
                    self.connection.get("password"),
                )

        try:
            jira = JIRA(**kwargs, max_retries=0)
            # Workaround for validating credentials as ``validate`` option on JIRA instance supports 
            # only cookie based authentication.
            # Command below requires user to be authenticated before use, otherwise exception is raised.
            jira.myself()
        except JIRAError as e:
            if "Client must be authenticated to access this resource" in e.text:
                raise Exception("Invalid credentials.")
            raise e


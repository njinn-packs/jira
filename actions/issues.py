import json

from .base import JiraAction


class CreateIssue(JiraAction):
    """
    Creates a Jira Issue
    """

    summary = None
    project = None
    issuetype = None
    reporter = None
    assignee = None
    parent_key = None
    description = ""

    def run(self):
        if not self.project:
            raise Exception("Project key is required")
        if not self.issuetype:
            raise Exception("Issue type is required")
        if not self.summary:
            raise Exception("Summary is required")

        issue_dict = {
            "project": {"key": self.project},
            "summary": self.summary,
            "description": self.description,
            "issuetype": {"name": self.issuetype},
        }

        if self.parent_key:
            issue_dict.update({"parent": {"key": self.parent_key}})

        if self.assignee:
            if self.assignee.find(":") == -1:
                print(f"Looking for accountId for {self.assignee}")
                res = self.jira.search_users(self.assignee)
                if not res:
                    raise Exception(f"Couldn't find user '{self.assignee}'")
                if len(res) > 1:
                    raise Exception(
                        f"There is more than one user matching '{self.assignee}' as assignee"
                    )
                print(f"accountId:{res[0].accountId}")
                issue_dict.update({"assignee": {"accountId": res[0].accountId}})
            else:
                issue_dict.update({"assignee": {"accountId": self.assignee}})

        new_issue = self.jira.create_issue(fields=issue_dict)

        return {"key": new_issue.key}


class TransitionIssue(JiraAction):
    """
    Transition a Jira Issue
    """

    key = None
    comment = None
    transition = None
    fields = None

    def run(self):
        issue = self.jira.issue(self.key)
        fields = dict()

        if self.fields:
            if self.fields.get("assignee"):
                fields["assignee"] = {"key": self.fields.get("assignee")}

            if self.fields.get("resolution"):
                fields["resolution"] = {"name": self.fields.get("resolution")}

            if self.fields.get("additional"):
                if isinstance(self.fields.get("additional"), str):
                    af = json.loads(self.fields.get("additional"))
                else:
                    af = self.fields.get("additional")

                for key, value in af.items():
                    fields[key] = value

        print(f"Moving issue {issue} to {self.transition}")

        if fields.get("resolution"):
            print(f"Setting resolution to {self.fields.get('resolution')}")

        if self.comment:
            print("Adding a comment:")
            print(self.comment)

        return self.jira.transition_issue(
            issue, self.transition, comment=self.comment, fields=fields
        )


class SearchIssues(JiraAction):
    """
    Search for Jira Issues with a JQL query
    """

    jql = None

    def run(self):
        if not self.jql:
            raise Exception("No JQL expression defined")

        print(f"Searching for '{self.jql}'")
        res = self.jira.search_issues(self.jql, maxResults=None)

        print(f"Found {len(res)} match(es)")
        issues = []
        for r in res:
            issues.append(r.key)

        return {"issues": issues}


class UpdateIssue(JiraAction):
    """
    Update Jira Issue
    """

    key = None
    summary = None
    assignee = None
    priority = None
    description = None
    additional = None
    comment = None

    def run(self):

        self.cloud = self.connection.get("cloud", False) if self.connection else True

        if not self.key:
            raise Exception("No issue key provided, don't know which issue to update.")
        issue = self.jira.issue(self.key)

        fields = dict()

        if self.assignee:
            if self.cloud:
                fields["assignee"] = {"accountId": self.assignee}
            else:
                print(f"Assigning issue to {self.assignee}")
                self.jira.assign_issue(issue, self.assignee)

        if self.summary:
            fields["summary"] = self.summary

        if self.description:
            fields["description"] = self.description

        if self.priority:
            fields["priority"] = {"name": self.priority}

        if self.additional:
            if isinstance(self.additional, str):
                af = json.loads(self.additional)
            else:
                af = self.additional

            for key, value in af.items():
                fields[key] = value

        print(f"Updating issue {self.key}")
        issue.update(fields)

        if self.comment:
            print("Adding comment")
            self.jira.add_comment(issue, self.comment)


class GetProjects(JiraAction):
    def run(self):
        return [str(p.key) for p in self.jira.projects()]

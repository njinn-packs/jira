from .base import JiraAction


class AddUser(JiraAction):
    """
    Add a User to Jira
    """

    username = None
    email = None
    fullname = None

    def run(self):

        if not self.username:
            raise Exception("No Username given")

        if not self.email:
            raise Exception("No EMail given")

        self.jira.add_user(self.username, self.email, fullname=self.fullname)

        return "Done"
